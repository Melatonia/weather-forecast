import React from 'react'
import Form from './components/Form'
import Weather from './components/Weather'

class App extends React.Component {
  state = {
    data: {},
    userInput: '',
    isSubmitted: false
  }
  getWeather = (userInput) => {
    return fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${userInput}&units=metric&appid=ecf7e163a3b70c016eae98245e189b9f`)
      .then(data => data.json())
      .then(data => {
        this.setState({ data })
      })
  }
  onChange = (event) => {
    this.setState({ userInput: event.target.value })
  }
  onSubmit = async (event) => {
    const { userInput } = this.state
    event.preventDefault()
    await this.getWeather(userInput)
    this.setState({ isSubmitted: true })

  }
  render() {
    return (
      <div className='wrapper'>
        {this.state.isSubmitted ? <Weather data={this.state.data} /> :
          <Form
            userInput={this.state.userInput}
            onChange={this.onChange}
            onSubmit={this.onSubmit} />
        }
      </div>
    )
  }
}

export default App
