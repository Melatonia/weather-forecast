import React from 'react'

class Weather extends React.Component {
    render() {
        const today = new Date();
        const format = { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' }
        return (
            <div className='weather'>
                <div className='date'> {today.toLocaleDateString("en-UK", format)} </div>
                <div className='location'> {`${this.props.data.city.name}, ${this.props.data.city.country}`} </div>
                <div className='weather-info'>
                    <div className='current-temperature'> {Math.round(this.props.data.list[0].main.temp)}{'\u00B0'} </div>
                    <div className='weather-details'>
                        <div className='description'> {this.props.data.list[0].weather[0].description} </div>
                        <div className='feels-like'>{`Currently the temperature feels like ${Math.round(this.props.data.list[0].main.feels_like)}\u00B0.`} </div>
                    </div>
                </div>
            </div >
        )
    }
}

export default Weather