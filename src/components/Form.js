import React from 'react'

class Form extends React.Component {
    render() {
        return (
            <div className='form'>
                <form onSubmit={this.props.onSubmit}>
                    <h2>Weather Forecast</h2>
                    <input
                        type='text'
                        value={this.props.userInput}
                        placeholder="City Name"
                        autoFocus={true}
                        required
                        onChange={this.props.onChange}
                    />
                </form>
            </div >
        )
    }
}

export default Form