This simple weather application was created with React and bootstrapped with Create React App.
 It uses the Open Weather Map API. Photo by Alex Machado on Unsplash.

Future Updates:
- Weekly Weather
- Autocomplete Suggestions
- Background Images based on current weather

[Link to Live Site](https://weather-forecast-1984.netlify.com/)
